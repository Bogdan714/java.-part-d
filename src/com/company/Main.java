package com.company;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nTask 1\n");
        Book[] books = new Book[100];
        for (int i = 0; i < books.length; i++) {
            books[i] = new Book();
            books[i].author = "Ludovic " + (10 + i);
            books[i].name = "Recipes, part " + i + 1;
            books[i].year = (i + 1) * 19;
        }
        int tempIndex = 0;
        for (int i = 1; i < books.length; i++) {
            if (books[i].year < books[tempIndex].year) {
                tempIndex = i;
            }
        }
        System.out.println("Автор самой старой книги: " + books[tempIndex].author);
        System.out.println("Введите имя автора для поиска (например 'Ludovic 42'): ");
        String toFind = scanner.nextLine();
        boolean found = false;
        System.out.println("Результаты поиска\n____________________");
        for (Book book : books) {
            if (book.author.equalsIgnoreCase(toFind)) {
                System.out.println(book.name + "\n----------");
                found = true;
            }
        }
        if (!found) {
            System.out.println("Книг с таким автором не найдено");
        }
        System.out.println("До какого года?");
        int year = Integer.parseInt(scanner.nextLine());//scanner.nextInt();
        found = false;
        System.out.println("Результаты поиска\n____________________");
        for (Book book : books) {
            if (book.year < year) {
                found = true;
                System.out.println(book.name);
                System.out.println(book.author);
                System.out.println(book.year);
                System.out.println("----------");
            }
        }
        if (!found) {
            System.out.println("Книг такого года не найдено");
        }

        System.out.println("\nTask 2\n");
        //declaration of banks
        Bank[] banks = new Bank[4];
        String[] names = {"FUIB", "PrivatB", "OschadB", "myBank"};
        //init banks
        for (int i = 0; i < 4; i++) {
            banks[i] = new Bank();
            banks[i].COURSE_EUR = 30.0f + (i * 0.5f);
            banks[i].COURSE_USD = 25.0f + (i * 0.5f);
            banks[i].COURSE_RUB = 0.25f + (i * 0.05f);
            banks[i].name = names[i];
        }

        //init scanner
        Scanner scan = new Scanner(System.in);

        //enter amount of money
        System.out.println("Enter the amount of money that you want to change: ");
        int amount = Integer.parseInt(scan.nextLine());

        //enter name of the bank
        System.out.println("Enter the bank " + Arrays.toString(names) + ":");
        String bankName = scan.nextLine();

        //convert uan to user currency
        boolean success = false;
        System.out.println("Result\n____________________");
        for (Bank bank : banks) {
            if (bank.name.equalsIgnoreCase(bankName)) {
                success = true;
                System.out.println("Bank \"" + bank.name + "\"");
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", "USD", amount / bank.COURSE_USD));
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", "EUR", amount / bank.COURSE_EUR));
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", "RUB", amount / bank.COURSE_RUB));
                System.out.println("--------------------");
            }
        }
        if (!success) {
            System.err.println("Can't find " + bankName);
        }
    }
}
